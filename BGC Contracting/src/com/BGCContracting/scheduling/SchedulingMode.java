package com.BGCContracting.scheduling;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.BGCContracting.*;
import com.BGCContracting.base.Page;

public class SchedulingMode extends Page{	 

	LoginPage lp = new LoginPage();
	SchedulingModeSearch SMS = new SchedulingModeSearch();

	@BeforeTest
	public void LaunchApplication(){
		lp.OpenWebsite(); 	
	}	

	//Login as an existing User	 
	@Test(priority=1) 
	public void DefaultUserlogin(){  
		System.out.println("Login as an existing User:");   		      
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		lp.LogOut();         
	}

	//Verify Search Result are displayed
	@Test(priority=2)
	public void VerifyValidSearchResult(){	
		System.out.println("Verify Search Result are displayed");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		//SMS.VerifySchedulingPage();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1","SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		lp.LogOut();
	}

	//Verify Invalid Search Result
	@Test(priority=3)
	public void VerifyInvalidSearchResult() {	
		System.out.println("Verify Invalid Search Result");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		//SMS.VerifySchedulingPage();
		SMS.Search(CONFIG.getProperty("InvalidSearchText"));
		SMS.VerifyInValidSearch(CONFIG.getProperty("InvalidSearchError"));
		lp.LogOut();
	}

	//Add Valid Schedule
	@Test(priority=4)
	public void AddValidSchedule() throws InterruptedException{	
		System.out.println("Add Valid Schedule");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		//SMS.VerifySchedulingPage();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1","SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		SMS.SelectSchedule();
		SMS.AddSchedulingShift(CONFIG.getProperty("ValidShift"));
		SMS.ClickDropdownShift();
		SMS.AddSchedulingDay( CONFIG.getProperty("ValidDay"));
		SMS.SaveScheduling();
		lp.LogOut();
	}
	//Verify Schedule
	@Test(priority=5)
	public void VerifySchedule() throws InterruptedException{
		System.out.println("Verify Schedule");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1", "SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		SMS.SelectSchedule();
		SMS.VerifyValidScheduling();
		lp.LogOut();
		
	}
	
	//Edit Schedule
	@Test(priority=6)
	public void EditSchedule() throws InterruptedException{
		System.out.println("Verify Schedule");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1", "SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		SMS.SelectSchedule();
		SMS.EditSchedulingShift(CONFIG.getProperty("ValidShift2"));
		SMS.ClickDropdownShift();
		SMS.EditScheduleDay(CONFIG.getProperty("ValidDay2") );
		SMS.SaveScheduling();
		lp.LogOut();
	}
	
	//Delete Schedule
	@Test(priority=7)
	public void DeleteSchedule() throws InterruptedException{
		System.out.println("Verify Schedule");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1", "SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		SMS.SelectSchedule();
		Thread.sleep(2000);
		SMS.DeleteSchedule();
		Thread.sleep(2000);
		SMS.SaveScheduling();
		lp.LogOut();
	}

	//Add Invalid shift Schedule
	@Test(priority=8)
	public void AddScheduleInvalidShift() throws InterruptedException{	
		System.out.println("Add Invalid shift Schedule");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		//SMS.VerifySchedulingPage();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1","SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		SMS.SelectSchedule();
		SMS.AddSchedulingShift(CONFIG.getProperty("InvalidShift"));
		SMS.AddSchedulingDay(CONFIG.getProperty("ValidDay"));
		SMS.SaveScheduling();
		SMS.verifyInvalidShiftScheduling();
		lp.LogOut();
	}

	//Add Invalid day Schedule and Verify Data Retention
	@Test(priority=9)
	public void AddScheduleInvalidDay() throws InterruptedException{	
		System.out.println("Add Invalid day Schedule and Verify Data Retention");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1", "SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		SMS.SelectSchedule();
		SMS.AddSchedulingShift(CONFIG.getProperty("ValidShift"));
		SMS.ClickDropdownShift();
		SMS.AddSchedulingDay(CONFIG.getProperty("InvalidDay"));
		SMS.SaveScheduling();
		SMS.VerifyInvalidDayScheduling();
		lp.LogOut();
	}
	
	//Verify Missing data error
	@Test(priority=10)
	public void AddScheduleMissingData() throws InterruptedException{	
		System.out.println("Verify Missing data error");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		SMS.NavigateSchedulingMode();
		SMS.Search(CONFIG.getProperty("ValidSearchText"));
		SMS.VerifyValidSearch("SchedulingModeSearchResult1", "SchedulingModeSearchResult2", CONFIG.getProperty("ValidSearchResult"));
		SMS.SelectSchedule();
		SMS.AddNewShift();
		Thread.sleep(500);
		SMS.DeleteDesc();
		SMS.SaveScheduling();
		SMS.VerifyMissingDataError();
		lp.LogOut();
	}
} 