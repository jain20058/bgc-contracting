package com.BGCContracting.scheduling;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.BGCContracting.*;


public class Projects extends SchedulingMode{

	LoginPage lp = new LoginPage();
	Project po = new Project();

	@BeforeTest
	public void LaunchBrowser(){
		lp.OpenWebsite();
	}

	//Add A new Sub Project using dynamic data
	@Test(priority=11)
	public void AddValidSubProject() throws InterruptedException{
		System.out.println("Add A new Sub Project");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.AddSubProject(CurrentDate1());
		po.SaveSubProject();
		Thread.sleep(2000);
		lp.LogOut();
		
		
		
	}
	
	//Verify Sub Project Added at its palace
	@Test(priority=12)
	public void VerifySubproject(){
		System.out.println("Verify Sub Project Added");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.VerifyValidSubProject();
		lp.LogOut();
	}
	
	//Edit Sub Project adding Dynamic data and verify that subproject is present at correct place
	@Test(priority=13)
	public void EditSubProject() throws InterruptedException{
		System.out.println("Edit Sub Project adding Dynamic data and verify that subproject is present at correct place");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.EditSubProject(CurrentDate2());
		po.SaveSubProject();
		po.NavigateProject();
		po.VerifyValidSubProject();
		po.VerifyEditedSubproject(CurrentDate2());
		lp.LogOut();
	}
	
	//Delete Subproject
	@Test(priority=14)
	public void DeleteSubProject() throws InterruptedException{
		System.out.println("Delete Subproject");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.DeleteSubProject();
		Thread.sleep(2000);
		po.SaveSubProject();
		lp.LogOut();
	}
	
	//Add Invalid Sub Project
	@Test(priority=15)
	public void AddInValidSubProject(){
		System.out.println("Add Invalid Sub Project");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.AddSubProject(CONFIG.getProperty("InvalidSubProjectName"));
		po.SaveSubProject();
		po.VerifyInValidSubProject();
		lp.LogOut();
	}

	//Add Valid Crew
	@Test(priority=16)
	public void AddValidCrew(){
		System.out.println("Add Valid Crew");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.AddCrew();
		po.EditCrewName(CONFIG.getProperty("NewCewName"));
		po.SaveCrew();
		po.NavigateProject();
		po.VerifyCrew();
		lp.LogOut();
	}
	
	//Add budget for Crew
	@Test(priority=17)
	public void AddBudget(){
		System.out.println("Add budget for Crew");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.AddBudget();
		po.SaveCrew();
		lp.LogOut();
	}
	
	//Delete Crew
	@Test(priority=18)
	public void DeleteCrew() throws InterruptedException{
		System.out.println("Delete Crew");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.VerifyCrew();
		po.DeleteCrew();
		po.SaveSubProject();
		Thread.sleep(5000);
		lp.LogOut();
	}

	//Add Invalid Crew
	@Test(priority=19)
	public void AddInvalidCrew() throws InterruptedException{
		System.out.println("Add Invalid Crew");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.AddCrew();
		po.EditCrewName(CONFIG.getProperty("Null"));
		Thread.sleep(500);
		po.SaveCrew();
		po.VerifyInvalidCrewName();
		po.CancelAddCrew();
		Thread.sleep(5000);
		po.NavigateProject();
		lp.LogOut();
	}
	

	//Add Site Administrators
	@Test(priority=20)
	public void AddSiteAdministrators(){
		System.out.println("Add Site Administrators");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.AddContentPublisher();
		lp.LogOut();
	}
	
	//Delete Site Administrators
	@Test(priority=21)
	public void DeleteSiteAdministrators() throws InterruptedException{
		System.out.println("Delete Site Administrators");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.DeleteContentPublisher();
		lp.LogOut();
	}
	
	//Add Site Superindent
	@Test(priority=22)
	public void AddSiteSuperindent() throws InterruptedException{
		System.out.println("Add and Delete Site Superindent");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.AddSiteSuperindent();
		lp.LogOut();
	}
	
	//Delete Site Superindent
	@Test(priority=23)
	public void DeleteSiteSuperindent() throws InterruptedException{
		System.out.println("Delete Site Superindent");
		lp.DoLogin(CONFIG.getProperty("Default_Username"), CONFIG.getProperty("Default_Password"));
		lp.SuccessfulLogin();
		po.NavigateProject();
		po.VerifyProjectPage();
		po.DeleteSiteSuperindent();
		lp.LogOut();
	}
}
