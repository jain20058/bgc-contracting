package com.BGCContracting;

import org.testng.Assert;

import com.BGCContracting.base.Page;


public class Project extends Page{
	
	String winHandleBefore = null;
	
	public void NavigateProject(){
		ClickXpath("Admin");
		ClickLink("Projects");
		ClickXpath("FirstProject");
	}
	
	public void VerifyProjectPage(){
		if(IsElementPresentXpath("ProjectName")){
			Assert.assertTrue(true);
		}
		else{
			Assert.fail();
		}
	}
	

	public void AddSubProject(String ProjectName) {
		ClickXpath("AddSubProject");
		InputXpath(OR.getProperty("ThirdSubProjectName"), ProjectName);
	}
	
	public void EditSubProject(String ProjectName) {
		ClearFieldXpath(OR.getProperty("ThirdSubProjectName"));
		InputXpath(OR.getProperty("ThirdSubProjectName"), ProjectName);
	}
	
	public void SaveSubProject(){
		ClickXpath("SaveSubProjectButton");
	}
	
	public void VerifyValidSubProject(){
		if(IsElementPresentXpath("ThirdSubProjectName")){
			Assert.assertTrue(true);
		}
		else{
			Assert.fail();
		}
		if(IsElementPresentXpath("AddedSubProject")){
			Assert.assertTrue(true);
		}
		else{
			Assert.fail();
		}
	}
	
	public void VerifyInValidSubProject(){
		if(IsTextPresent(CONFIG.getProperty("InvalidProjectNameError"))){
			Assert.assertTrue(true);
		}
		else{
			Assert.fail();
		}
	}
		
	public void DeleteSubProject(){
		ClickXpath("DeleteSubProject");
	}
	
	public void AddCrew(){
		ClickXpath("AddCrewFirst");
	}
	
	public void EditCrewName(String NewCrewName){
		ClearFieldXpath(OR.getProperty("CrewName"));
		InputXpath(OR.getProperty("CrewName"), NewCrewName);
	}
	
	public void SaveCrew(){
		ClickXpath("SaveCrew");
	}
	
	public void VerifyCrew(){
		if(IsElementPresentXpath("VerifyCrew")){
			Assert.assertTrue(true);
		}
		else{
			Assert.fail();
		}
	}
	
	public void DeleteCrew() throws InterruptedException{
		ClickXpath("VerifyCrew");
		Thread.sleep(3000);
		ClickXpath("DeleteCrew");
	}
	
	public void VerifyInvalidCrewName(){
		if(IsTextPresent(CONFIG.getProperty("InvalidCrewError"))){
			Assert.assertTrue(true);
		}
		else{
			Assert.fail();
		}
	}
	
	public void CancelAddCrew(){
		ClickXpath("CancelCrew");
	}

	public void AddContentPublisher() {
		ClickXpath("ContentPublisher");
		navigateNewFrame();
		ClickXpath("AddContentPublisher");
		InputXpath(OR.getProperty("InputContentPublisher"), CONFIG.getProperty("ContentPublisherName"));
		ClickXpath("SelectFirstContentPublisher");
		ClickXpath("OkContentPublisher");
		returnMainWindow();
		ClickXpath("SaveButton");		
	}
	
	public void navigateNewFrame(){
		
		try{
			
				driver.switchTo().frame(0);
		
		}catch(Exception e){
			Assert.fail();
		}
		Assert.assertTrue(true);
	}
	//Return to main window
	public void returnMainWindow(){
		
		try{
			
			driver.switchTo().defaultContent();
		}catch(Exception e){
			Assert.fail();
		}
		Assert.assertTrue(true);
	}

	public void DeleteContentPublisher() throws InterruptedException {
		ClickXpath("ContentPublisher");
		navigateNewFrame();
		ClickXpath("DeleteContentPublisher");
		Thread.sleep(500);
		ClickXpath("OkContentPublisher");
		returnMainWindow();
		ClickXpath("SaveButton");
	}

	public void AddSiteSuperindent() throws InterruptedException {
		ClickXpath("SiteSuperindent");
		navigateNewFrame();
		ClickXpath("AddSiteSuperindent");
		InputXpath(OR.getProperty("InputSiteSuperindent"), CONFIG.getProperty("SiteSuperindentName"));
		Thread.sleep(2000);
		ClickXpath("SelectFirstSiteSuperident");
		ClickXpath("OkSiteSuperident");
		returnMainWindow();
		ClickXpath("SaveButton");
	}

	public void DeleteSiteSuperindent() throws InterruptedException {
		ClickXpath("SiteSuperindent");
		navigateNewFrame();
		ClickXpath("DeleteSiteSuperident");
		Thread.sleep(500);
		ClickXpath("OkSiteSuperident");
		returnMainWindow();
		ClickXpath("SaveButton");
	}

	public void AddBudget() {
		ClickXpath("VerifyCrew");
		ClickXpath("AddBudget");
		navigateNewFrame();
		InputXpath(OR.getProperty("BudgetDay"), CONFIG.getProperty("BudgetDay"));
		ValueFromDropdownValue(OR.getProperty("BudgetMonth"), CONFIG.getProperty("BudgetMonth"));
		InputXpath(OR.getProperty("BudgetYear"), CONFIG.getProperty("BudgetYear"));
		ClickXpath("SaveBudget");
		returnMainWindow();
	}

	public void VerifyEditedSubproject(String date) {
		String ProjectName = GetTextDynamicXpath(OR.getProperty("AddedSubProject"));
		if(ProjectName.equals(date)){
			Assert.assertTrue(true);
		}
		else
		{
			Assert.fail();
		}
	}

}
